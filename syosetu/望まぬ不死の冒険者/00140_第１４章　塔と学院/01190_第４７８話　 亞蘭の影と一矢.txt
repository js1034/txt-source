那麼，瓦薩的能力大致上已經理解了


當然，他也有和我一樣，還藏著後手的可能性。可如果太在意這一點而不敢發起攻擊，就太優柔寡斷了


所謂戰鬥，總是有風險相伴的

但只有明知如此，也能勇敢向前的人，才能取得最後的勝利......大概吧

「差不多該重新開始了......要上咯」

雖然沒必要事先提醒，但瓦薩看上去受到了不小的衝擊，有點發愣的樣子

直接進攻會比較有利，但事後被抱怨的話......感覺會很麻煩

還是盡可能堂堂正正從正面取勝吧


不過，我即使受了致命傷，也能很快完全恢復，感覺也算不上多堂堂正正就是了

「......！來吧！」

瓦薩被我的聲音嚇了一跳，臉上愣愣的表情也消失了，用力握緊槍喊道

架勢很不錯呢

雖然不知道他是怎麼想的，但看來是下定了決心，不管我的身體有多詭異，也要贏下這場比賽


聽到他的聲音後，我再次向瓦薩衝了過去

用正經的姿勢突進


雖然對手是人類，使用超出常識的姿勢能夠起到一定的迷惑作用，但不可否認的是，由於姿勢過於勉強，只能使出七成的力氣

用氣和魔力進行強化，才總算是能在實戰中派上用場

但若要正面戰鬥，還是我成為冒險者以來不斷磨練的劍術更加可靠


我的攻擊被對瓦薩易看穿，用槍彈開了

然後他順勢放下槍尖，向我的胸口直刺過來

雖然現在空門大開......但我立即誇張地扭轉身體，避開了這一擊

「......又來啊......！？」

雖說是第二次，但在對人戰中，還是很難預料到的吧


看來瓦薩還是無法適應，一時間迷茫起來，似乎不知該如何是好

我上半身後仰，把手伸向地面，然後抬起腳來， 變成了倒立的姿勢，瞄準停在空中的槍頭，一腳將其踢開


然後借著慣性翻了個跟頭，腳一著地，我立刻猛踏地面，與瓦薩拉近距離

趁著他還沒來得及把槍收回中段


他的視線捕捉到了我

但現在已經來不及阻止了吧


潛入到瓦薩懷中的我，反手握緊劍，狠狠向他的胸口橫掃過去


但不知為何

——鏘！

發出了尖銳的聲音


定睛一看，只見瓦薩的胸前覆蓋著一塊金屬板

他之前穿在身上的只有輕便皮鎧，是沒有帶著金屬板的


那麼，這就是用異能產生的了

仿彿要證明這一點似的，金屬板在我眼前改變了形狀，變成許多小箭，向我射了過來


雖然比之前的短劍小了不少，但數量眾多，無法全部避開

在這樣的距離下，法扭轉身體躲開，也來不及閃避或後退了，我向右後方退去

避開了一部分小箭，但肩膀還是被射中了


即便如此，我身上還穿著長袍

這件長袍的防禦力可是打包票的，畢竟從來沒有破損過

那種程度的攻擊，能夠輕鬆彈開


看到我後退，瓦薩主動攻擊了過來

用力揮舞起槍

先是一招縱劈，然後追著我躲開的方向使出橫掃，我彎腰避開後，緊跟著又是下段突刺

來不及用劍架開了

旋轉的槍尖向我扎過來，如果命中的話，一定會刺得很深吧

雖然想要進行迴避，但金屬箭又浮現在空中，瞄準了我可能閃避的方向


真令人鬱悶

不得不承認，在這種場合下，瓦薩的能力相當便利啊


要麼選槍，要麼選箭

總之要被刺一下

從威力上考慮的話，選箭那邊好一些吧

就主動迎上去好了

不那樣的話，就無路可逃了

「......呣！？」

被扎一下也無大礙


因為是我這樣的身體，才能做出這種動作，瓦薩似乎也沒有預想到

雖然吃了一驚，但並沒有像之前那樣愣住


要是愣住就能抽身而退了吶......

我用左半身撞上了瓦薩的金屬箭，但因為是主動上前的，箭還來不及加速，威力比想像的還低，只是稍微刺了一下而已


好機會，我這樣想到，迅速迴轉劍鋒，瞄準瓦薩的槍尖，向劍中注入了氣力

接近到這種地步，雖然相當危險，但同時也是最佳的機會

更何況，我剛被箭射中，這下瓦薩就沒辦法再使出異能進行防禦了吧


從剛才的戰鬥看來，不把之前生成的金屬消掉，似乎就沒辦法生成新的金屬

雖然也可能是假裝出來的，但這裡就應該賭一把

實際上，我也確實猜中了......

注入氣力的劍命中了槍尖靠下一些的部分，切入進去，斬斷了槍頭

「......可惡！？」

瓦薩大叫著，停止攻勢，迅速拉開了距離

......總而言之，先報了一箭之仇

我這樣想到

已經破壞了對方的武器，這下就算我贏了吧，稍微這樣想了。但瓦薩看起來還沒失去鬥志


戰鬥，還在繼續......