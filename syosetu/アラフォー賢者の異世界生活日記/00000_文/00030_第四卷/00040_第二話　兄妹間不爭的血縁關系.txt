最近的庫洛伊薩斯很忙碌，同時心情也很好。

一方面是整理了幾乎化為他個人房間的研究室，一方面是他和聖捷魯曼派的年輕研究員候補生們一起在研究魔法術式，而最近這研究的進展也很順利。

雖然因為這緣故，他幾乎都關在研究室裡……

而他心情好的時候，簡直可說果然是泡在研究室裡的時候。也就是他比平常更常待在研究室裡，然而在體力方面也因此有些不足之處。簡單來說就是缺乏運動。

雖然他也差不多該練點體力了，但是他是一次只能集中在一件事情上的類型，所以並不想被多餘的事情打擾。

而更是讓他心情好的理由，應該是和哥哥與妹妹之間的關係吧。

從小開始就每天埋首於魔法研究的庫洛伊薩斯，很不擅長和身為下任當家，不斷鑽研學問又外向的茨維特相處。反正公爵家會由哥哥來繼承，他便縮在家裡，結果雙方變得意見不合，愈來愈討厭對方。

對於妹妹瑟雷絲緹娜，他則是認定她不會使用魔法，當不成魔導士，所以根本不在討論範圍內，對她毫無興趣。然而知道那個妹妹其實和自己一樣是個喜歡研究的人之後，庫洛伊薩斯對於至今為止自己過於冷落她的態度也有所反省。

他道歉其實算是快的了。他不像茨維特那樣在道歉前煩惱了好幾次、還變得很形跡可疑。他的個性率直，立刻就道歉了。他這種不會執著於做錯的事情，想立刻修正錯誤的態度，說起來也非常像個研究者。

不過從根本上來說，因為毫無興趣就完全無視瑟雷絲緹娜這點，作為一個人而言還真是不知道該怎麼說他。

而他和兄妹間的關係有所改善後，最近和他們的對話也讓他很是開心。

像是茨維特說的「考慮到要有效地運用魔法，去研究魔法的特性這一點應該是對的。要是不理解魔法，怎麼能思考戰術啊。」這件事。

瑟雷絲緹娜也說了「不只是為了戰鬥，魔法應該有更多不同的運用方法才對。我想製作能夠在人們的生活中派上用場的魔法！可以的話也想試著製作魔導具。」這樣的話。

雖然彼此的方向性不同，但和從不同角度切入魔法研究的兩位兄妹對話實在非常的有意義，也能帶給他刺激，讓他獲得新的發現。

他是個徹頭徹尾的研究狂。

「在你心情好的時候提起這件事真是抱歉～不過下周就是實戰訓練嘍？你不先鍛鍊一點體力起來可以嗎？」

馬卡洛夫的一句話讓他的動作停了下來。

到剛剛心情都還很好的庫洛伊薩斯動作硬生生地停下來之後，以像是機器人般僵硬的動作，把頭轉向了馬卡洛夫。而且他的臉上掛著非常嫌惡的表情。

「你為什麼現在要提這件事？我好不容易把它拋諸腦後忘記了……」

「不對，不能忘記吧！你不是得強制參加嗎？而且就算你想忘記，那一天也絕對會到來的吧。」

「庫洛伊薩斯，你都準備好了嗎～？只靠學院指定的裝備會很不安心的喔～？」

「伊•琳……庫洛伊薩斯不可能會準備吧？因為他除了研究之外基本上是個廢人啊……」

雖然這些話說得有些過分，但基本上他們說的都是真的。

庫洛伊薩斯的外表和內在的反差實在太大了。雖然有著銀髮、高瘦俊美且給人聰慧印象的外表，實際上卻是個運動白痴，平常的生活也是足以被加上「超」來形容的邋遢。要是伊•琳沒有犧牲奉獻自己來照顧他，他的宿舍房間應該只要過上幾天就會變成垃圾屋了。

儘管周遭的人都因他的外表而擅自做了許多幻想，但真正的庫洛伊薩斯除了研究以外完全不行。以這層面來說他的兄妹們像樣多了。

雖然令人有些意外，不過茨維特會把周遭環境打理得非常整潔，甚至到了有些潔癖的程度。瑟雷絲緹娜則是不會把任何多餘的東西留在身邊。

以這年紀的少女來看，瑟雷絲緹娜的房間會令人覺得「這是怎麼回事？」，完全不像女孩子的房間。畢竟她的房裡連個布偶都沒有。

因為房間裡什麼都沒有，實在太冷清了，連蜜絲卡都看不下去，準備了鮮花來裝飾。

扯得雖然有些遠了，不過這冷清的房間最近也因為增加了許多收納了藥草和礦物的瓶子，變成了一個有研究者感覺的房間。儘管如此，仍能斷言這個房間絕對不會變成像庫洛伊薩斯那樣的垃圾屋。

「你啊，這個月已經讓多少僕役逃走了？為什麼打掃完的隔天又會堆滿垃圾啊，太奇怪了吧！」

「就算你這麼說……我是有自己反覆以想要嘗試的藥草調配了一整夜的印象啦。但因為我是個研究者，這種事情應該還在可以接受的範圍內吧？」

「你的狀況根本太超過了啦！那惡臭都飄到隔壁房間的我這邊來了喔？我醒來後就發現自己躺在醫務室裡了，你到底做了什麼啊！」

「我也不記得了。我回過神來時發現自己倒在宿舍的中庭裡，真的不知道發生了什麼事……」

「調配藥水這種事情在研究室裡做啦啊啊啊啊啊啊！」

要重新說清楚的話，就是庫洛伊薩斯在試著調配針對精神系魔法用的回復藥水，結果卻讓房裡充滿了惡臭，本人在逃往中庭的時候喪失了意識。

而他調配出的試做藥水散發出奇怪顏色的煙霧，蔓延在整棟宿舍內，隔壁房間的馬卡洛夫雖然就那樣暈過去了，但距離污染地區稍遠處的學生們則是做出了奇怪的舉動，現場一片混亂。

像是笨蛋一樣大笑個不停，或是在現場脫個精光都還算是好的，其中也有做出令人不敢以言語描述的可怕行為，或是慷慨激昂地訴說BL和GAY雖然很像但完全不同的人，可說依據對象不同而發揮出了各式各樣的效果。

負責救助這些被害者的人是這樣說的：「那是地獄……太可怕了……人類居然可以糟糕到那種程度。可以的話真希望這些景象能從我的記憶中消除。感覺我都要變得不對勁起來了……」。就算看到「危險！禁止混合」也會若無其事地去做的男人，這就是庫洛伊薩斯。

對此實在無法詳細說明，能說的只有救護小組闖入時看見的全是必須打上馬賽克的危險景象。

那不是精神狀態正常的人類該知道的內容。

「雖然我還記得因為我無論如何都很想試試看，所以沒能忍住……不過真在意產生了怎樣的效果呢。可惜連一點紀錄都沒能留下來。」

「我也因為很在意所以去問了……聽說被救助的那些人在現場都變得很奇怪喔？你肯定做出了很糟糕的東西……」

「感覺你哪天會不小心把國家給毀滅呢……庫洛伊薩斯。」

「瑟琳娜～就算是庫洛伊薩斯也……可能……會做出這種事呢……」

這群伙伴已經完全將庫洛伊薩斯視為麻煩製造者了。

優秀卻欠缺思慮，不會分辨哪些事是不該做的，搞出問題時周遭便會受到嚴重的損害。

光是本人沒有惡意這點就夠糟了，而且不知為何被害者大多都喪失了相關記憶，完全不記得發生了什麼事。如果是愉快犯已經足以依法處置了，然而目前這一切都被當成事故來處理。

畢竟不管他創造出的藥品有什麼功效，隔天就會一點痕跡都不剩的消失，所以不會留下任何證物。他究竟做出了什麼，至今還是個謎團。

「所以說，結果庫洛伊薩斯你的裝備打算怎麼辦～？」

「沒辦法了……跟哥哥借他備用的裝備來穿吧。」

「你這話是認真的嗎？以前我借你的裝備過了一個月後生銹又發霉了喔？我是沒差啦，但你哥會毫不留情地痛揍你一頓吧。」

「感覺真的會變成那樣呢。畢竟是和庫洛伊薩斯有血緣關係的兄弟，我想應該不會客氣的。」

「庫洛伊薩斯……好好整理一下自己周遭的環境比較好喔～？」

光是他本人有印象的範圍內，就無話反駁了。

借來的東西好一陣子都不還，有時東西還給主人時還已經嚴重受損了。

因為他過了很久才會想起來，等到想起來的時候東西已經呈現在各種意義上都為時已晚的狀態，有些東西還就這樣永遠消失了。

大叔或伊莉絲要是看到他的房間，一定會說「這是腐海之森……」吧。

瑟雷絲緹娜的朋友卡洛絲緹有一次曾造訪他的房間，然而在開門的瞬間便尖叫且失去了意識。

沒人知道她到底看到了些什麼，她也忘卻了當時的記憶。

只是不知為何，從那天之後她再也不去庫洛伊薩斯的房間了。

「卡洛那傢伙到底看到了什麼？那是你的房間，你不記得自己放了些什麼了嗎？」

「……老實說，那陣子我都睡在研究室，很久沒回宿舍去了。」

「我找小卡洛一起去接庫洛伊薩斯，她便忽然全身顫抖，最後還哭了喔？想必是看到了非常可怕的東西呢～」

「庫洛伊薩斯……你沒有製造人工生命體出來吧？那可是首屈一指的危險生物……根據我聽到的傳聞，有人聽到『好想早點變成人類喔～！』的聲音從你的房間傳出來喔？」

「「那是怎樣！是說，已經太遲了嗎？」」

「我沒印象……我有做出那種神秘的生物嗎？」

庫洛伊薩斯•泛•索利斯提亞。十七歲。

和父親在不同方面上是個充滿謎團的人物。而且也是個毫無自覺的危險人物。

他所住的房間簡直是腐海。是會產生出不知道到底是什麼奇怪生物的危險房間。

順帶一提，在諸國間是禁止創造生命的。

◇　◇　◇　◇　◇　◇　◇

將時間拉回兩個月前，進入暑假的學生宿舍──某天的深夜裡，宿舍的其中一間房內。

房間的主人庫洛伊薩斯今天也睡在研究室裡沒有回來。

在窗簾緊閉，沒有光線透入的宿舍房內，不知名的某種生物今天也開始胎動著。

黏液狀的那個玩意在黑暗中蠢動著，為了尋求自由，開始像史萊姆一樣從瓶內爬了出來。

沒有固定形狀，感覺很噁心的軀體搖晃著，最後黏液逐漸構築成了身體。

簡直像是蛹中的東西正在變化成它該有的樣子。

彷佛在快速播放的情況下看著生命進化的過程。從單細胞生物，變成了以複數體細胞構成的生物……

那個東西最後化為了擁有人型的生物，然而樣貌極為醜惡。

以長著三根手指的雙臂打開窗戶後，那個生物跨入了籠罩在黑暗中的戶外，不知消失在何方。唯一目擊其身影的人，只記得那生物有條長長的尾巴。

沒人知道那生物究竟是什麼……也無從得知。

那生物在某個城裡的一角成了都市傳說，僅有少數人會談起……

然後。

──嘰呀啊啊啊啊啊啊啊啊！

……現在，在沒有任何人會靠近的地下水道中，神秘的生物正大聲咆嘯著。

創造者完全不知道這生物誕生的經過，也把創造出這生物的過程給忘得一乾二淨。

真相全在被遺忘的遠方……與其這麼說，不如說這是個在偶然之下誕生的生物，然而一切都消失在黑暗中了。

◇　◇　◇　◇　◇　◇　◇

話題拉回來。雖然在研究室裡已經被大家說不行了，但庫洛伊薩斯最後還是去找了茨維特。陪他一起去的是馬卡洛夫。

兩人想辦法找到了茨維特，把事情經過告訴了他。可是……

「……就是這樣，可以的話希望能跟你借一下裝備……」

「我拒絕！你覺得聽了這種話，我真的會借給你嗎？你的神經到底多大條啊！」

庫洛伊薩斯抱著些許的期望，為了借用裝備而向茨維特低頭……其實也沒低頭，只是提出這個請求。他只是想說或許可行，總之問看看，但果然被拒絕了。

這也是庫洛伊薩斯自作自受吧。

順帶一提現在他們在大圖書館裡，茨維特的朋友迪歐也在現場。

雖然迪歐的目的不說大家也應該知道，不過他是為了和他的初戀對象瑟雷絲緹娜成為朋友才會出現在這裡的。不過他完全沒思考過那個凶惡的戀愛病發作的事情。

再把話題拉回來，庫洛伊薩斯來借用裝備的結果就是這樣。要是把東西借給本來就很邋遢的庫洛伊薩斯，不知道什麼時候才會回到自己手裡。不如說東西很有可能根本不會再回來。

要是聽到這種話之後還會說「OK，就借你吧！」，那人想必是個不得了的聖人君子吧。

不會把借的東西還回去、會遺失、會弄壞、會丟掉、會被不知名的生物拿走，庫洛伊薩斯是個不只三種要素，甚至備齊了五種要素的少見人才。

把東西借給他，跟把東西送給他是一樣的意思。

「你啊……你自己的裝備怎麼了？之前那套應該是跟我的差不多時間做的啊……」

「雖然我有把它們挖出來，但全都爛掉了。看起來生銹得很厲害，皮革的部分還有被什麼咬過的痕跡……」

「挖出來？而且還被咬了？這個學院應該有做好讓老鼠等小動物不會靠近的防範措施喔？有什麼東西會咬爛裝備啊！」

「誰知道？有被銳利的牙齒撕裂，還有被高濃度的酸性液體溶解的痕跡喔？」

「你……在宿舍裡養了什麼啊？那很明顯不是小動物做的吧？」

這兩人從以前開始就沒什麼交集，最近增加了不少對話的機會雖然是好事，但愈了解庫洛伊薩斯，就愈覺得他很神秘，而且非常危險。

在有許多學生一起生活的宿舍內反覆進行不知名的實驗，這可是前所未聞的事。

茨維特聽到這些事都覺得煩惱得想抱住頭了。不對，實際上他已經抱著頭了。

「我就說了吧？不可能的……」

「馬卡龍……你要好好監視這傢伙。不知道他會幹出什麼事來。」

「誰是馬卡龍啊！而且監督這個問題兒童是你這個做哥哥的該做的事情吧！」

「沒辦法……我負擔不起。」

「不要把這種麻煩的弟弟推給我啦！」

在吵成一團的茨維特和馬卡洛夫旁，庫洛伊薩斯正一臉事不關己的樣子，悠哉的想著『哎呀？要是沒有裝備的話，說不定我就不用參加這個戶外教學了？』這種正合他意的發展。

不過學院的活動可不是這麼簡單就能躲過的，就算沒有裝備也會被調去做後勤支援，所以無論如何都得參加。事情沒他想得那麼美。

「庫洛伊薩斯你還是老樣子呢……可是不能不去參加實戰訓練喔？畢竟成績優秀的人必須強制參加。」

「……這樣啊。事情沒有這麼順利的呢。是說……你是瓦力嗎？」

「不是啦！我是迪歐，我們中等學部時同班啊，你忘了嗎！」

「啊……是叫這個名字啊。抱歉這麼失禮，作為道歉，讓我送個石鬼面具給你吧。只要濺上一點血就會長出奇怪的刺來，是非常稀有的面具……」

「我不需要！那什麼奇怪的面具啊！」

「我偶然在古董市場上買到，不知道是做什麼用途的。你要不要試著戴戴看呢？」

「你是想拿別人來做人體實驗嗎！」

庫洛伊薩斯有收集癖，偶爾去鎮上時會買些奇怪的東西回來。

其中又以魔導具一類的東西特別多，雖然他是打算哪天要研究才買了一大堆，但最後都沒研究，就這樣堆在宿舍裡。

結果那些東西成了腐海的基礎，在那種跟儲藏室一樣的房間內反覆進行調配魔法藥的實驗後，危險的房間便完成了。

不可思議的是就算睡在這樣的房間裡，庫洛伊薩斯本身也不覺得有什麼問題。就算覺得有，也只會裝模作樣的說句「哎呀哎呀，有些亂了呢」，完全沒有打算要收拾。

剛剛那些奇怪的魔導具就這樣一直沉眠在那個有如垃圾場般的房間內。

「「為什麼你一臉事不關己的樣子啊！」」

「不，我只是想說在後方待機的話，不需要裝備也無所謂吧……」

「怎麼可能啊！就算在後方也有可能會被魔物襲擊喔！」

「庫洛伊薩斯……你為什麼都覺得事情會順著你的意思發展啊？若卡力可很辛苦，你也稍微體諒一下人家吧。」

「我是馬卡洛夫啦啊啊啊啊啊啊啊啊啊啊啊啊！只有一個字是對的吧！」

「不要在意這種小事。」

「這才不是小事吧！我們同班吧！是同組的吧！喂！」

「真的是記不住他人名字的兄弟呢～……」

迪歐深深嘆了口氣。

這對兄弟完全沒想要記住不重要的人物的名字。雖然有必要的話會記一下，但只要一陣子沒碰面就會立刻忘記。

就算是薩姆托爾，等到派系內的事情塵埃落定后也會被徹底遺忘吧。

以某方面來說這對兄弟的個性也是滿大剌剌的。

還好大圖書館內沒其他使用者在，但是圖書館的管理員們似乎覺得他們很擾人，瞪著他們。

很會給人添麻煩的傢伙們齊聚一堂，這個混亂的辯論又持續了好一陣子。

◇　◇　◇　◇　◇　◇　◇

在索利斯提亞兄弟爭執的時候，瑟雷絲緹娜正在訓練場教烏爾娜魔法。

說是這麼說，但因為獸人族能刻劃在潛意識內的魔法術式數量有限，所以要有效運用的話，必須先選出適合她的魔法才行。

事情就變成瑟雷絲緹娜到剛剛為止都在透過實戰訓練觀察，在注重格闘戰的情況下挑選了適合的魔法。

「基於各種考量後，我想讓烏爾娜學習『魔力護盾』、『氣流領域』、『鷹眼』這幾個魔法。」

「為什麼是這三個？不是以格闘戰為主來考量嗎？」

「可以藉由將『魔力護盾』施放在手臂或腳上，來讓身體化為武器。『氣流領域』則是為了從敵人的遠距離攻擊下保護自己。『鷹眼』則是為了盡早確認敵人的位置及狀況。」

「護盾魔法可以當作武器嗎？」

「要不要試試看？」

瑟雷絲緹娜在手臂上施展了「白銀神壁」，將其化為銳利的劍型後，垂直地朝著立在訓練場中用來當作魔法標的物的木偶揮下。

木偶漂亮地分成了兩半，裝在上頭的老舊鎧甲掉落在地，發出了聲響。周圍的學生們不知道發生了什麼事而愣在原地。

護盾魔法意外的可以運用在許多層面。其實在劍上附加魔法，用來提升攻擊力和耐久性的魔法也運用了護盾魔法的魔法術式。以比較粗略的分法來說，就算說附加魔法是添加了便於使用的魔法術式的護盾魔法也沒什麼問題。然而遺憾的是烏爾娜的才能並不足以使用附加魔法。由於種族的特性，她只能學會簡單的魔法。

正是因為這樣瑟雷絲緹娜才會選擇活用護盾魔法。只要把護盾魔法纏繞在手臂上，就可以強化打擊的威力，再搭配上烏爾娜的身體能力，攻擊力想必不可小覷。雖然這招在根本上有著「魔力必須足以持續下去」的限制──

在她們周圍的人對於這個可說是改變了既有想法的創新念頭全都啞口無言。

「這就是護盾魔法擁有的可能性。我想只要把這個纏繞在手臂上，就能成為強力的打擊型武器了。不過我沒辦法教你我剛剛使用的魔法，用普通的防御魔法應該也能做到類似的效果吧。」

「好厲害！原來利用屏障魔法能夠做到這種事情啊！」

「以獸人特有的身體強化搭配利用屏障魔法的打擊。手上還持有武器的話，應該可以打倒大多數的魔物。但是不可以太過於信賴自己的力量呢，要是被多數的魔物給包圍還是很危險的。」

「啊～……因為很多緣故，我其實沒有格闘戰的經驗呢～頂多只有跟人打架的程度，這樣無法打倒魔物吧～」

接著便開始了魔法訓練。老實說烏爾娜的成績很差。

不靠著參加野外實戰訓練賺點分數，就有留級的危險。可是實戰方面等同於外行，就算現在開始做為了保身的訓練，也不知道烏爾娜能夠使用魔法到什麼程度。

儘管如此，為了降低讓性命暴露在危險下的可能性，還是比什麼都不做來得好。

她們並未使用大叔修改過的魔法術式，而是採用瑟雷絲緹娜以研究為目的，自行最佳化後的魔法術式。由於杰羅斯經手過的魔法正經由老家販售，不可以不小心就擴散出去，而要讓比較偏獸人族血統的烏爾娜發動這所學院中使用的魔法又有困難。

獸人族特有的技能「闘獸化」由於會消耗大量魔力，對身體的負擔也很大，不能隨意使用。所以才會以增加可用招式為優先。

烏爾娜將魔法術式刻劃在潛意識內後，立刻試著使用。

「呃……魔力啊，化為擋下敵人的盾吧。『魔力護盾』。」

發動魔法後，烏爾娜的周圍出現了魔力的屏障。

這雖然是參考杰羅斯改良後的魔法，由瑟雷絲緹娜重新構成的廉價版，但烏爾娜也毫無困難的發動了。雖然魔力消耗量差了點，也比較難操控，但這些負擔對於增加魔力量以及作為習得「操縱魔力」技能的訓練上而言是恰到好處。

瑟雷絲緹娜本身是覺得改得有點失敗，不過只看結果的話是十分出色的作品。

「請試著把展開在周遭的魔法屏障聚集到手臂上。如果辦不到，我們就從操控魔力的訓練開始做起吧。」

「嗯，我試試看……哦？這個……好像有點難呢？」

「咦？『有點』……？」

魔法屏障逐漸集中到了烏爾娜的手臂上，像是要藏住她的手臂似地凝聚在一起。明明是初次操作，速度卻快得嚇人。

瑟雷絲緹娜也辦得到，然而那是在經過了兩個月的猛烈特訓，以及回到學院後也不斷練習操控魔力才能有的成果。而且就算是這樣，速度也沒那麼快。

可是烏爾娜在沒有任何事前知識的情況下便輕易做到了，而且操作還異常的精準且迅速。

這就是所謂的種族特性，獸人族需要有效率的使用天生就不多的魔力，所以為了消耗魔力的使用量，會本能的去操控魔力。更何況降低了所需魔力的魔法術式盡可能地減輕了她的負擔，還利用了環境中的魔力，所以效果極佳。

也就是說，雖然他們持有的魔力比人類少，但生來便擁有凌駕於所有種族之上的魔力操控能力。若是持有的魔力量和人類相同，獸人族應該會是遠勝於人類的種族吧。對於必須拚命練習操控魔力的人來說，這真是令人羨慕的能力。

魔法屏障在烏爾娜的手臂上形成了半透明的魔力臂甲，她正反覆張開、握緊那只手確認其手感。當事者完全不知道自己做了多厲害的事情。

瑟雷絲緹娜儘管有些困惑，仍想以訓練場中的木偶確認其威力。

「那、那麼就來試試看這個的威力吧。」

「嗯♪只要去揍那個就行了吧？」

「對，可以的話是希望你能連身體強化都一起用上一次，來觀察效果……」

「那我試試看！」

「咦？」

說時遲那時快，烏爾娜忽然強化了身體，朝著木偶猛衝過去，以纏繞有魔力屏障的手臂痛快地揍了下去。

獸人的身體能力極為驚人，木偶當場粉碎。

旁邊看到的人們全都驚訝地張大了嘴，過於衝擊的景象讓他們完全說不出話。

烏爾娜也是出了名的吊車尾，所以學生們看待她的眼光也不是很好，然而這個結果一舉顛覆了她吊車尾的印象。

「好厲害喔，瑟雷絲緹娜大小姐♪真沒想到會有這等威力！」

「咦？嗯……我也沒想到你這麼快就能靈活運用了……」

「這樣不管什麼魔物都一擊就能搞定了呢。」

「因為是處在不斷消耗魔力的狀態下，所以把這個當成絕招來用會比較好。現在你的魔力應該也持續在消耗著喔。」

「啊，真的耶……總覺得頭好像有點暈……」

「快點解除魔法！不然你會昏倒的！」

護盾魔法和身體強化似乎會帶來很大的負擔。

雖然透過訓練學會了魔法的使用方式，但要靈活運用，等級恐怕還是太低了，魔力消耗得很快。想要同時使用，無論如何都需要提升等級。

「先持續做操控魔力的訓練一陣子，在實戰訓練時提升等級吧。不然依照現況來看只會耗盡魔力昏過去而已……」

「這就是用盡魔力的感覺啊……你體驗過嗎？我是第一次。啊哈哈哈哈。」

「抱歉在你們開心談天時打擾了。」

「「嗚呀啊！」」

眼鏡冰山美人女僕突然現身。

她果然還是沒讓人感受到半點氣息，以簡直像是某必殺系列的人一樣出現在身後，不知為何有些得意地以骨骼會變得很奇怪的姿勢站著。

現在仍是一副感覺會從背後拿出什麼東西的樣子。

「蜜、蜜絲卡……拜託不要嚇我們。」

「我又……完全沒有感覺到她的氣息……氣味也是。」

「因為我每天都有使用體香劑。比起那個，大小姐，老主人寄信來了。」

「爺爺寄來的？」

瑟雷絲緹娜接過蜜絲卡遞來的信，信看來早就已經拆封了，她輕鬆的拿出了裡面的信紙。打開信紙準備閱讀的瞬間，她忽然起了疑心，看向蜜絲卡。

「蜜絲卡……你沒有看這封信吧？」

「那當然。反正他一定是寫了『老夫好寂寞喔～要死了』之類的噁心內容給大小姐吧。畢竟他一直以來都是這樣，都事到如今了，我也沒興趣。」

「蜜絲卡……你其實很討厭爺爺吧？」

「我打從心底愛著老主人喔？比世界上的任何人都……應該吧？」

「為什麼是疑問句？」

雖然心中有很多思緒，但瑟雷絲緹娜決定總之先看信後，蜜絲卡說的話成真了。

完全不是那個年紀的老人該寫出的內容寫了整整三張信紙，最後才以極為失禮的方式寫了重要的事情。不如說明明那個重要的事情才是主題，信裡面卻完全沒寫到詳細的內容。

那個重要的事情，內容是「最後跟你說，杰羅斯先生要去當實戰訓練的護衛～……老夫想去的說，哭哭（淚）」。

瑟雷絲緹娜不禁猛然趴在地上。

「大小姐，這樣很難看喔？」

「爺爺……這是最重要的事情吧。不能這樣下去，得趕快去告訴哥哥……」

「茨維特少爺在圖書館喔。烏爾娜小姐就由我來負責摸摸……不，由我來照顧，大小姐您趕快去告訴少爺吧。」

「拜託你了，事不宜遲啊！」

「等、等一下，瑟雷絲緹娜大小姐……這個人，有點可怕……」

確定瑟雷絲緹娜已經全力奔向大圖書館後，蜜絲卡露出了可疑的笑容。

眼鏡的光芒令人感到恐懼。

「等等，總覺得……很可怕耶……」

「沒事的，沒什麼好怕的喔？馬上就會結束了……唔呵呵呵呵。」

不用說，沒過多久訓練場便響起了哀號聲。

烏爾娜那毛茸茸的尾巴被人給摸到爽了。

◇　◇　◇　◇　◇　◇　◇

「……差不多該做個結論了。我去拿素材來，庫洛伊薩斯去修理裝備。現在做的話應該還來得及。」

「應該也沒有別的辦法了～不過，把素材交給庫洛伊薩斯沒問題嗎？」

「根據我的猜測，我想他一定會私吞這些素材，不會拿去修理裝備。因為那可是庫洛伊薩斯喔？」

「「感覺很有可能……」」

「我知道你們三個是以怎樣的目光看我的了。真是的，我當然……不會做那種事啊。」

「「「騙人！你剛剛那停頓是怎樣！」」」

大圖書館中，這些人依然在討論裝備的事。

若是魔物的素材，庫洛伊薩斯很有可能會興奮到忘記原本的目的。

畢竟茨維特提供的素材來自於棲息在法芙蘭大深綠地帶的魔物。庫洛伊薩斯肯定不會拿這些素材去修理裝備，而會拿去當作調配魔法藥的材料吧。

只要扯上魔法，庫洛伊薩斯便會不分對象的出手。

「你們一點都不信任我呢……我也是有理性的喔？在這種走投無路的狀況下，我怎麼可能會做那種事。」

「真的嗎？那要是我手上有奇美拉的毒針呢？」

「我給你錢，請你賣給我！現在馬上！賣給我吧！」

「「你這不是馬上就做了嗎！哪裡有理性啊！」」

「我就知道會變成這樣。畢竟是庫洛伊薩斯啊……」

庫洛伊薩斯的物欲完全不知節制。

「你啊，把毒針放在滿是垃圾的房間裡，不小心踩到的話，真的會出人命喔？」

「要是因此而死也正合我意！作為研究者完全可以接受這種死法。畢竟這樣就可以確認奇美拉的毒有什麼效果了。」

「不行……庫洛伊薩斯根本有病。」

「外表看起來明明很冷酷……內在實在太遺憾了。」

明明是這麼糟糕的人，卻不知為何很受歡迎，讓旁邊三人的心境十分複雜。甚至感覺到這世界有多麼不講理……

「哥哥……呼……呼……」

全力奔跑過來的瑟雷絲緹娜氣喘吁吁地向茨維特搭話。

雖然沒必要這麼急，但只要扯上尊敬的老師，她就會變得很積極。

「瑟雷絲緹娜？怎麼了，喘成這樣。」

「老、老師他……要來……」

「「「「啥？」」」」

「老師好像要來擔任實戰訓練的護衛。」

空氣瞬間靜止了。

關於瑟雷絲緹娜所說的老師，簡單來說就是大叔，但除了兩位學生之外，其他人並不知道他的為人。

「老師？是你們的師傅對吧？教了你們魔法術式的解讀方法，以及教會妹妹魔法的……」

「嗯……有時會以拳頭戰鬥的魔導士。」

「到底是怎樣的魔導士啊……超乎常理也該有個限度。」

「啊……啊……瑟雷絲緹娜小姐……這是夢嗎？」

雖然有個陷入了戀愛病中的人，但庫洛伊薩斯和馬卡洛夫大略的問了一下那位鍛鍊了茨維特和瑟雷絲緹娜的魔導士的事情。而那位魔導士將參加學院的活動。

「……是老爸安排的嗎？如果是的話……感覺事情很可疑啊。」

「為什麼你會這樣想？他說不定是為生活所苦，所以才接了傭兵的工作來賺錢啊。」

「師傅要多少賺錢方法就有多少，沒必要特地來接這種沒什麼賺頭的工作。你能獨自打倒七只飛龍嗎？」

「辦不到。這根本是去找死。」

「對吧？既然如此，答案就很有限了吧。恐怕是來保護我的吧？血統主義的那些笨蛋有什麼動作了嗎？」

跟剛剛不同，茨維特的眼神變得十分險惡。

掌握了惠斯勒派約半數的薩姆托爾，有傳聞說他和地下組織的人有往來。

由於茨維特的父親德魯薩西斯也擁有自己獨立的諜報部，所以他判斷應該是消息傳到了父親的情報網那裡。

「不是突然想來看看我們的狀況嗎？」

「這也有可能……畢竟他的個性很差啊～」

「咳咳！茨、茨維特……」

「啊……」

茨維特看到行動有些怪異的迪歐，便知道他在要求什麼了。

儘管覺得很麻煩，他仍無可奈何地把朋友介紹給瑟雷絲緹娜。要不然迪歐每天都會死纏爛打的催促他，實在煩人。

「瑟雷絲緹娜，這是我之前跟你提過的朋友。金色短髮的這位則是庫洛伊薩斯的朋友。」

「啊，抱歉失禮了。我有從哥哥那邊聽聞過兩位的事，我記得名字是……迪彭哥•波葛羅和馬格利姆茲利吧。」

「「這女孩也記錯名字了？那個名字是哪來的啊！」」

儘管三兄妹走上了不同的道路，記不住無關緊要的人的名字這點倒是很像。

這時候迪歐也知道自己對於她來說是個無關緊要的人了。

這兩個人還以為只有妹妹會好些，然而還是敵不過血緣啊……

在那之後迪歐總算想辦法讓她記住了自己的名字。不過是這點小事，迪歐仍非常歡欣鼓舞，開心得讓茨維特都傻眼了……

◇　◇　◇　◇　◇　◇　◇

「這樣啊，虫子終於來到了緹娜身邊……呵呵呵……」

「該如何處置呢？」

「咕呼呼……這還用說嗎？當然要把他給烤熟。沒錯，烤得恰到好處……」

「唉……請別把我給牽扯進去喔？還請您自己負擔起這個責任。」

「什麼話，只要不被發現就好了。丹迪斯……沒錯，只要不被發現……」

敗壞的老人，也就是克雷斯頓終於開始準備了。

溺愛孫女的笨爺爺不知為何用心地以磨刀石磨著小刀。而且臉上還帶著愉快到顯得凶惡的笑容……

還沒有人知道迪歐的命運究竟會變得如何。

他究竟能不能活下去呢。